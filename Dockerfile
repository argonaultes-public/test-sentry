FROM python:3.11

WORKDIR /app

COPY requirements.txt .
RUN pip install -r /app/requirements.txt

COPY . .
EXPOSE 8000

RUN python manage.py migrate

ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]