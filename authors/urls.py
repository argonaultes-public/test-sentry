from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("log", views.log_message, name="log"),
    path("division", views.division, name="division"),
    path("author", views.add_author, name="author"),
    path("list", views.get_author, name="list")
]