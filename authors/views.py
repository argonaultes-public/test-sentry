from django.http import HttpResponse
from sentry_sdk import capture_exception, capture_message
import json
from .models import Author

# Create your views here.
def index(request):
    try:
        raise json.JSONDecodeError('this is an error of json decode', 'because', 1)
    except json.JSONDecodeError:
        capture_exception()
        return HttpResponse('oops')

def log_message(request):
    capture_message(message='file not found', level='error')
    return HttpResponse('log_message')


def add_author(request):
    first_name = request.GET['first_name']
    author = Author.objects.create(first_name=first_name)
    return HttpResponse(author.id)

def get_author(request):
    try:
        author_id = int(request.GET['id'])
        author = Author.objects.get(id = author_id)
        return HttpResponse(author.first_name)
    except:
        capture_exception()
        return HttpResponse('not found', status = 500)

def division(request):
    division = 1 / 0
    return HttpResponse('division')