// Creator: Grafana k6 Browser Recorder 1.0.4

import { sleep, group, check } from 'k6'
import http from 'k6/http'

export const options = {
  stages: [
    { duration: '10s', target: 10 },
    { duration: '15s', target: 10 },
    { duration: '5s', target: 0 },
  ],
  thresholds: {
    // http errors should be less than 1%
    'http_req_failed': ['rate<0.1'],
  }
}

export default function main() {
  let response

  group('page_1 - http://${__ENV.MY_HOSTNAME}:8000/authors/', function () {
    response = http.get(`http://${__ENV.MY_HOSTNAME}:8000/authors/`, {
      headers: {
        host: `${__ENV.MY_HOSTNAME}:8000`,
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:127.0) Gecko/20100101 Firefox/127.0',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        'accept-language': 'en-US,en;q=0.5',
        'accept-encoding': 'gzip, deflate, br, zstd',
        dnt: '1',
        'sec-gpc': '1',
        connection: 'keep-alive',
        cookie:
          'csrftoken=KaZ9kYJ6fF9rxx7HcecBVKzUoIfrLBPV; __wt1vpc=dipInstanceId%3Dmvjyek4m-e138231a68ad82f054e3d756c6634ba1-7309261463dbe004c95f0a0312fd8b27%26installId%3DpVcoad05AFsLjgLDNgLnIQ3r%26dssLicenseKind%3DCOMMUNITY%26bkdDistrib%3Dcentos%26bkdDistribVersion%3D7%26regChannel%3Dvirtualbox-image%26deploymentMode%3DCUSTOM%26nodeType%3Ddesign%26isAutomation%3Dfalse%26dssVersion%3D11.1.3%26vdssUser%3D92668751%26tutorial-project%3Dtrue%26tutorial-id%3DDKU_TUTORIAL_NLP_VISUAL; __wt1vic=c5c14da131bfdee',
        'upgrade-insecure-requests': '1',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
      },
    });

  })

  sleep(5)

  group('page_2 - http://${__ENV.MY_HOSTNAME}:8000/authors/log', function () {
    response = http.get(`http://${__ENV.MY_HOSTNAME}:8000/authors/log`, {
      headers: {
        host: `${__ENV.MY_HOSTNAME}:8000`,
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:127.0) Gecko/20100101 Firefox/127.0',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        'accept-language': 'en-US,en;q=0.5',
        'accept-encoding': 'gzip, deflate, br, zstd',
        dnt: '1',
        'sec-gpc': '1',
        connection: 'keep-alive',
        cookie:
          'csrftoken=KaZ9kYJ6fF9rxx7HcecBVKzUoIfrLBPV; __wt1vpc=dipInstanceId%3Dmvjyek4m-e138231a68ad82f054e3d756c6634ba1-7309261463dbe004c95f0a0312fd8b27%26installId%3DpVcoad05AFsLjgLDNgLnIQ3r%26dssLicenseKind%3DCOMMUNITY%26bkdDistrib%3Dcentos%26bkdDistribVersion%3D7%26regChannel%3Dvirtualbox-image%26deploymentMode%3DCUSTOM%26nodeType%3Ddesign%26isAutomation%3Dfalse%26dssVersion%3D11.1.3%26vdssUser%3D92668751%26tutorial-project%3Dtrue%26tutorial-id%3DDKU_TUTORIAL_NLP_VISUAL; __wt1vic=c5c14da131bfdee',
        'upgrade-insecure-requests': '1',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
      },
    })
  })

  sleep(5)

  group('page_3 - http://${__ENV.MY_HOSTNAME}:8000/authors/test', function () {
    response = http.get(`http://${__ENV.MY_HOSTNAME}:8000/authors/test`, {
      headers: {
        host: `${__ENV.MY_HOSTNAME}:8000`,
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:127.0) Gecko/20100101 Firefox/127.0',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        'accept-language': 'en-US,en;q=0.5',
        'accept-encoding': 'gzip, deflate, br, zstd',
        dnt: '1',
        'sec-gpc': '1',
        connection: 'keep-alive',
        cookie:
          'csrftoken=KaZ9kYJ6fF9rxx7HcecBVKzUoIfrLBPV; __wt1vpc=dipInstanceId%3Dmvjyek4m-e138231a68ad82f054e3d756c6634ba1-7309261463dbe004c95f0a0312fd8b27%26installId%3DpVcoad05AFsLjgLDNgLnIQ3r%26dssLicenseKind%3DCOMMUNITY%26bkdDistrib%3Dcentos%26bkdDistribVersion%3D7%26regChannel%3Dvirtualbox-image%26deploymentMode%3DCUSTOM%26nodeType%3Ddesign%26isAutomation%3Dfalse%26dssVersion%3D11.1.3%26vdssUser%3D92668751%26tutorial-project%3Dtrue%26tutorial-id%3DDKU_TUTORIAL_NLP_VISUAL; __wt1vic=c5c14da131bfdee',
        'upgrade-insecure-requests': '1',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
      },
    });
    check(response,
      {
        'is status 404': (r) => r.status === 404,
      }
    )
  })

  // Automatically added sleep
  sleep(5)

  group('page 4 - add new author', function() {
    const id = Math.round(Math.random() * 1000)
    response = http.get(`http://${__ENV.MY_HOSTNAME}:8000/authors/author?first_name=author${id}`)
    check(response,
      {
        'author created 200': (r) => r.status === 200,
      }
    );
    check(response,
      {
        'author id': (r) => r.body.match('/[0-9]+/'),
      }
    );
  });
  sleep(2);
  group('page 5 - get random author id', function() {
    const id = Math.round(Math.random() * 1000)
    response = http.get(`http://${__ENV.MY_HOSTNAME}:8000/authors/list?id=${id}`)
    check(response,
      {
        'author exists': (r) => !r.body.includes('not found'),
      }
    )    
  });
  sleep(2)

}
